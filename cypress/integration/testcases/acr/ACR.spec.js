/// <reference types="cypress" />


import LoginPageActions from '../../../pageobjects/pageactions/login/LoginPageActions'
import CommonPageActions from '../../../pageobjects/pageactions/common/CommonPageActions'
import ConfigurationPageActions from '../../../pageobjects/pageactions/acr/ConfigurationPageActions'
import AcrEntryPageActions from '../../../pageobjects/pageactions/acr/AcrEntryPageActions'
const or = require('../../../locators/acr_locators.json');
const common = require('../../../locators/common_locators.json');

    const loginPage = new LoginPageActions()
    const commonPage = new CommonPageActions()
    const configurationPage = new ConfigurationPageActions()
    const acrEntryPage = new AcrEntryPageActions()



    before(()=>{

        cy.fixture('acr_data').then((data)=>{

            globalThis.data = data

        })

    })

    beforeEach(()=>{

        loginPage.navigateToURL()


    })

///কনফিগারেশন  > এসিআর অন্তর্ভুক্তি > DELETE EMPLOYEE
describe("Working ACR module ACR inclusion Employee Delete ",()=>{
    
    it("TC_1. PE: ACR inclusion Delete Employee",()=>{
        cy.login(data.validLoginCredentials.sujit.username,data.validLoginCredentials.sujit.password);

        commonPage.getDashboardAvatar(common.dashboardPage.hrmAvatar)
        //এসিআর
        commonPage.getLeftNavMenu(or.leftNavmenuItems.disciplineInvestigation, data.leftNavMenu.acr)
        //কনফিগারেশন
        commonPage.getLeftNavMenu(or.leftNavmenuItems.disciplineInvestigation, data.leftNavMenu.configuration)
        //এসিআর অন্তর্ভুক্তি
        commonPage.getLeftNavSubMenu(or.leftNavmenuItems.acrInclusion,data.leftNavMenu.acrSubMenu.acrInclusion)
        //Search Employee 
        commonPage.getSearchListWithSelectorByName(
            common.GlobalLocator.searchByName,
            data.configuration.searchEmployee
        )

       ///Delete Employee
       commonPage.getDeleteBtn(common.GlobalLocator.deleteBtn)
       commonPage.getCompleteBtn(common.GlobalLocator.completeBtn3)

    })
})

///কনফিগারেশন  > এসিআর অন্তর্ভুক্তি  > ADD EMPLOYEE
describe("Working ACR module ACR inclusion Employee Add ",()=>{
    
    it("TC_1. PE: ACR inclusion Add Employee",()=>{
        cy.login(data.validLoginCredentials.sujit.username,data.validLoginCredentials.sujit.password);
 
        commonPage.getDashboardAvatar(common.dashboardPage.hrmAvatar)

        //এসিআর
        commonPage.getLeftNavMenu(or.leftNavmenuItems.disciplineInvestigation, data.leftNavMenu.acr)
        //কনফিগারেশন
        commonPage.getLeftNavMenu(or.leftNavmenuItems.disciplineInvestigation, data.leftNavMenu.configuration)
        //এসিআর অন্তর্ভুক্তি
        commonPage.getLeftNavSubMenu(or.leftNavmenuItems.acrInclusion,data.leftNavMenu.acrSubMenu.acrInclusion)
        
        //AddBtn 
        commonPage.addBtn(common.GlobalLocator.addBtn)
        //branch মডিউল মেইনটেনেন্স শাখা 
        configurationPage.branchNameAndSearch(
            data.configuration.branchName,
            common.GlobalLocator.searchBtn2
            )

        //Search Employee 
        commonPage.getSearchListWithSelectorByName(
            common.GlobalLocator.searchByName,
            data.configuration.searchEmployee
        )
        /// Add ACR Person      
        configurationPage.addACRPersonDetails(data.configuration.addACR.person1)    

        //// অন্তর্ভুক্ত করুন
        commonPage.addBtn(common.GlobalLocator.completeBtn2)


    })
})

/// এসিআর এন্ট্রি > এসিআর আবেদন সংরক্ষণ,অনুমোদনের জন্য পাঠানো
describe.only("Working ACR module ACR Application, Save, send for approval",()=>{
    
    it("TC_1. PE: ACR Application, Save, send for approval",()=>{
        cy.login(data.validLoginCredentials.sujit.username,data.validLoginCredentials.sujit.password);

        commonPage.getDashboardAvatar(common.dashboardPage.hrmAvatar)
        //এসিআর
        commonPage.getLeftNavMenu(or.leftNavmenuItems.disciplineInvestigation, data.leftNavMenu.acr)

        //এসিআর এন্ট্রি
        commonPage.getLeftNavSubMenu(or.leftNavmenuItems.acrEntry,data.leftNavMenu.acrSubMenu.acrEntry)
        
        
        //branch মডিউল মেইনটেনেন্স শাখা 
        configurationPage.branchNameAndSearch(
            data.configuration.branchName,
            common.GlobalLocator.searchBtn2
            )

        //Search Employee 
        commonPage.getSearchListWithSelectorByName(
            common.GlobalLocator.searchByName,
            data.configuration.searchEmployee
        )

        //ViewBtn 
        commonPage.getviewBtn(common.GlobalLocator.viewBtn)

        //AddBtn 
        commonPage.addBtn(common.GlobalLocator.addBtn)

        ///এসিআর আবেদন
        acrEntryPage.AcrApplicationFormDetails()
    })
})




afterEach(() => {
    cy.logout(common.logoutPage.logoutDropdownBtn2,common.logoutPage.logoutBtn2)
})