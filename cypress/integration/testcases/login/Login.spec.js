/// <reference types="cypress" />

import LoginPageActions from '../../../pageobjects/pageactions/login/LoginPageActions'
import CommonPageActions from '../../../pageobjects/pageactions/common/CommonPageActions'

const or = require('../../../locators/dp_locators.json');
const common = require('../../../locators/common_locators.json');
describe("check login with credentials",()=>{

    const loginPage = new LoginPageActions()
    const commonPage = new CommonPageActions()
    before(()=>{

        cy.fixture('dp_data').then((data)=>{

            globalThis.data = data

        })

    })

    beforeEach(()=>{

        loginPage.navigateToURL()


    })

    it("invalid login credentials",()=>{
        cy.login(data.invalidLoginCredentials.username,data.invalidLoginCredentials.password);
        commonPage.getValidationMessage(common.loginPage.errorMsg, data.loginErrorMessage.errorMessage)
        commonPage.pageWait(data.pageWait.short)

    })

    it("valid login credentials",()=>{
        cy.login(data.validLoginCredentials.morshed.username,data.validLoginCredentials.morshed.password);
        commonPage.pageWait(data.pageWait.short)
        cy.logout(common.logoutPage.logoutDropdownBtn,common.logoutPage.logoutBtn)
    })


    // afterEach(() => {
    //     cy.logout()
    //   })


})