// ***********************************************
// This example commands.js shows you how to
// create various custom commands and overwrite
// existing commands.
//
// For more comprehensive examples of custom
// commands please read more here:
// https://on.cypress.io/custom-commands
// ***********************************************
//
//
// -- This is a parent command --
// Cypress.Commands.add('login', (email, password) => { ... })
//
//
// -- This is a child command --
// Cypress.Commands.add('drag', { prevSubject: 'element'}, (subject, options) => { ... })
//
//
// -- This is a dual command --
// Cypress.Commands.add('dismiss', { prevSubject: 'optional'}, (subject, options) => { ... })
//
//
// -- This will overwrite an existing command --
// Cypress.Commands.overwrite('visit', (originalFn, url, options) => { ... })
require('@4tw/cypress-drag-drop')
import 'cypress-file-upload';
require('cypress-downloadfile/lib/downloadFileCommand')
import LoginPageElements from '../pageobjects/pageelements/login/LoginPageElements'
import CommonPageElements from '../pageobjects/pageelements/common/CommonPageElements'
//-- This is the create page object --
// ***********************************************
//Common create page object 
const loginPage = new LoginPageElements()
const commonPage = new CommonPageElements() 

//-- This is Login Method --
Cypress.Commands.add("login", (userName, password) => 
{
    loginPage.userNameField().type(userName).should('have.value', userName)
    cy.wait(1000)
    loginPage.passwordField().type(password).should('have.value', password)
    cy.wait(1000)
    loginPage.getEnterButton().should('have.text', 'প্রবেশ করুন').click()
    cy.wait(4000)
})

//-- This is LogOut method --  
Cypress.Commands.add("logout", (userAvatarSelector,exitBtnSelector) => 
{

    commonPage.getUserAvatar(userAvatarSelector).click({force: true})
    cy.wait(1000)
    commonPage.getExitLink(exitBtnSelector).click({force: true})
    cy.wait(1000)
})

/*
Cypress.Commands.add("waitAndClick", (selector) => 
{
    try{
        selector.waitForExist();
        selector.click();

    }catch(error){
       throw new Error(`could not click on selector: ${selector}`);
    }
})

Cypress.Commands.add("waitAndSetText", (selector,text) => 
{
    try{
        selector.waitForExist();
        selector.setValue(text);

    }catch(error){
       throw new Error(`could not typeText on selector: ${selector}`);
    }
})
*/