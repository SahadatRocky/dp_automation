
/// <reference types="cypress" />
const or = require('../../../locators/common_locators.json');
export default class LoginPageElements{
     
    userNameField(){  // ব্যবহারকারীর নাম field
  
        return cy.get(or.loginPage.username)
    }

    passwordField(){  //পাসওয়ার্ড field

        return cy.get(or.loginPage.password)
    }

    getEnterButton(){  //প্রবেশ করুন button
        return cy.get(or.loginPage.enterBtn)

    }    
}