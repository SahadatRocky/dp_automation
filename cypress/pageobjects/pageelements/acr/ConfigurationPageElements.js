
/// <reference types="cypress" />
const or = require('../../../locators/acr_locators.json');
export default class ConfigurationPageElements{
 
    branchField(){
        return cy.get(or.configuration.branch)
    }

}