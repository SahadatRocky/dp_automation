/// <reference types="cypress" />

import ConfigurationPageElements from '../../../pageobjects/pageelements/acr/ConfigurationPageElements'
import CommonPageElements from '../../../pageobjects/pageelements/common/CommonPageElements'
export default class ConfigurationPageActions{

    constructor(){

        globalThis.configurationpageelement = new ConfigurationPageElements()
        globalThis.commonpageelement = new CommonPageElements()
    }

    branchNameAndSearch(
        branchName,
        searchBtn2Selector
        ){
            configurationpageelement.branchField().click({force: true})
            cy.wait(1000)
            commonpageelement.DropdownItem().contains(branchName).click({force: true})
            cy.wait(1000)

            commonpageelement.getSearchBtn(searchBtn2Selector).click({force: true})
        }

    /////Select ACR Person
        addACRPersonDetails(name){
        
            cy.get("mat-card-content:visible tr td:nth-child(2)").each(($el,index, $list)=> {
    
                let employeeNameText = $el.text()
                if(employeeNameText.includes(name)){
                    cy.get("mat-card-content:visible tr td:nth-child(2)").eq(index).prev().find(".mat-checkbox-layout .mat-checkbox-inner-container").click()
                }
            })
            cy.wait(2000)
    
         }    

}